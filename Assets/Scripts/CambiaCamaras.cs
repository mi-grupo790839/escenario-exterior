using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambiaCamaras : MonoBehaviour
{
    public Camera camera1;
    public Camera camera2;
    public Camera camera3;
    public Camera camera4;
    public Camera camera5;
    int iCamaraChange = 0;
    void Start()
    {
        camera1.enabled = true;
        camera2.enabled = false;
        camera3.enabled = false;
        camera4.enabled = false;
        camera5.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

        if(Input.GetKeyDown(KeyCode.C)) {

            if (iCamaraChange == 0)
            {
                camera1.enabled = false;
                camera2.enabled = true;
                camera3.enabled = false;
                camera4.enabled = false;
                camera5.enabled = false;

                iCamaraChange++;


            }
            else if (iCamaraChange == 1)
            {
                camera1.enabled = false;
                camera2.enabled = false;
                camera3.enabled = true;
                camera4.enabled = false;
                camera5.enabled = false;

                iCamaraChange++;
            }
            else if (iCamaraChange == 2)
            {
                camera1.enabled = false;
                camera2.enabled = false;
                camera3.enabled = false;
                camera4.enabled = true;
                camera5.enabled = false;

                iCamaraChange++;
            }
            else if (iCamaraChange == 3)
            {
                camera1.enabled = false;
                camera2.enabled = false;
                camera3.enabled = false;
                camera4.enabled = false;
                camera5.enabled = true;

                iCamaraChange++;
            }
            else
            {
                camera1.enabled = true;
                camera2.enabled = false;
                camera3.enabled = false;
                camera4.enabled = false;
                camera5.enabled = false;

                iCamaraChange = 0;
            }
        }
    }
}
